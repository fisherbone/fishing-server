<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>

<html class="fixed sidebar-left-collapsed">
    <head>
        <title>Places</title>
    </head>
    <body>
    <h2 class="panel-title">Places</h2>
                        <div>
                            <table>
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Имя</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${placeList}" var="place">
                                        <tr>
                                            <td>${place.id}</td>
                                            <td>${place.title}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
    </body>
</html>
