<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>

<html class="fixed sidebar-left-collapsed">
    <head>
        <title>Fishes</title>
    </head>
    <body>
    <h2 class="panel-title">Fishes</h2>
                        <div>
                            <table>
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Имя</th>
                                        <th>Translit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${fishList}" var="fish">
                                        <tr>
                                            <td>${fish.id}</td>
                                            <td>${fish.name}</td>
                                            <td>${fish.translit}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
    </body>
</html>
