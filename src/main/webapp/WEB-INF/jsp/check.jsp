<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Check webservices</title>
    </head>
    <body>
        <script type="text/javascript">
            function bytesToBase64(imgobj,bytes) {
                // Convert a byte array to a base-64 string
                var result="";
                var str = "data:image/png;base64,";
                var base64map = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";        
                // Use browser-native function if it exists
                if (typeof btoa === "function") result=str+btoa(Binary.bytesToString(bytes));
                for(var base64 = [], i = 0; i < bytes.length; i += 3) {
                        var triplet = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
                        for (var j = 0; j < 4; j++) {
                                if (i * 8 + j * 6 <= bytes.length * 8)
                                        base64.push(base64map.charAt((triplet >>> 6 * (3 - j)) & 0x3F));
                                else base64.push("=");
                        }
                }
                if (result.length === 0) result=str+base64.join("");
                imgobj.src = result;
            }
            </script>
        <h1>There is all get methods:</h1><br>
        <form name="Test" method="post" action="check">            
            <c:forEach var="method" items="${methods}">              
                <input type="radio" name="r" value='<c:out value="${method}"/>'>
                <c:out value="${method}"/>
                <br>
            </c:forEach> 
            <button type="submit">Check</button>
        </form>
        <div>
            <%if (request.getAttribute("methodResults") != null) {%>
            <table align="center" cellspacing="2" cellpadding="2" border="0" width="100%">
                <th>id</th><th>name</th><th>image</th>
            <c:forEach var="result" items="${methodResults}">              
                <tr bgcolor='#c0c0c0' align=center>
                <td>${result.id}</td>
                <td>${result.name}</td>
                <td><img src="data:image/png;base64,${result.imageBase64}" alt=""/></td> 
                <!--
                <td><img onload="javascript:bytesToBase64(this,${result.image})" src="" /></td>
                -->
                </tr>
            </c:forEach> 

                </table>
            <%}%>
        </div>
    </body>
</html>
