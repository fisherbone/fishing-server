package ua.com.fishing.rest.controller;

public class PlaceRestURIConstants {
    public static final String DUMMY_PLACE = "/rest/place/dummy";
    public static final String GET_PLACE = "/rest/place/{id:^\\d+$}";
    public static final String GET_COUNT = "/rest/place/count";
    public static final String GET_ALL_PLACE = "/rest/place/all";
    public static final String GET_PLACE_RANGE = "/rest/place/{first:^\\d+}\\D{second:\\d+$}";
    public static final String CREATE_PLACE = "/rest/place/create";
    public static final String DELETE_PLACE = "/rest/place/delete/{id}";
}
