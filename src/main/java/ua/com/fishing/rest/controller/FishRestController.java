package ua.com.fishing.rest.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ua.com.fishing.mvc.exception.NotFoundException;
import ua.com.fishing.orm.controller.FishRepository;
import ua.com.fishing.orm.entity.Fish;

@RestController
@RequestMapping(value = "/rest/fish/**", produces = "application/json")
public class FishRestController {

    private static final Logger LOG = Logger.getLogger(Fish.class.getName());
    @Autowired
    private FishRepository fishRep;

    @RequestMapping(value = FishRestURIConstants.DUMMY_FISH, method = RequestMethod.GET)
    public @ResponseBody
    Fish getDummyFish() {
        LOG.log(Level.INFO, "Start getDummyFish");
        Fish fish = new Fish();
        fish.setName("Dummy");
        return fish;
    }

    @RequestMapping(value = FishRestURIConstants.GET_FISH, method = RequestMethod.GET)
    public @ResponseBody
    Fish getFish(@PathVariable Integer id) {
        System.out.println("Start getFish. ID=" + id);
        Fish fish = fishRep.getById(id);
        return fish;
    }

    @RequestMapping(value = FishRestURIConstants.GET_COUNT, method = RequestMethod.GET)
    public @ResponseBody
    Integer getCount() {
        System.out.println("Start getCountFish.");
        return fishRep.getCount();
    }

    @RequestMapping(value = FishRestURIConstants.GET_ALL_FISH, method = RequestMethod.GET)
    public @ResponseBody
    List<Fish> getAllFishs() {
        LOG.log(Level.INFO, "Start getAllFishs.");
        return fishRep.getAll();
    }

    @RequestMapping(value = FishRestURIConstants.GET_FISH_RANGE, method = RequestMethod.GET)
    public @ResponseBody
    List<Fish> getRangeFishs(@PathVariable Integer from, @PathVariable Integer count) {
//        System.out.println("From: " + from);
        LOG.log(Level.INFO, "Start getRangeFishs.");
        return fishRep.getRange(from, count);
    }
    /*
     @RequestMapping(value = FishRestURIConstants.CREATE_FISH, method = RequestMethod.POST)
     public @ResponseBody
     Fish createFish(@RequestBody Fish fish) {
     LOG.log(Level.INFO, "Start createFish.");
     fishRep.create(fish);
     return fish;
     }

     @RequestMapping(value = FishRestURIConstants.DELETE_FISH, method = RequestMethod.PUT)
     public @ResponseBody
     Fish deleteFish(@PathVariable("id") int fishId) {
     LOG.log(Level.INFO, "Start deleteFish.");
     Fish fish = fishRep.getById(fishId);
     fishRep.delete(fish);
     return fish;
     }
     */

    @RequestMapping
    public void get404() throws NotFoundException {
        throw new NotFoundException("Page not found");
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView getNotFound(NotFoundException ex) {
        ModelAndView mav = new ModelAndView("notfound");
        mav.addObject("message", ex.getMessage());
        return mav;
    }
}
