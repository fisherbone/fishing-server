package ua.com.fishing.rest.controller;

public class FishRestURIConstants {
    public static final String DUMMY_FISH = "/rest/fish/dummy";
    public static final String GET_COUNT = "/rest/fish/count";
    public static final String GET_FISH = "/rest/fish/{id:^\\d+$}";
    public static final String GET_FISH_RANGE = "/rest/fish/range/{from}/{count}";
    public static final String GET_ALL_FISH = "/rest/fish/all";
    public static final String CREATE_FISH = "/rest/fish/create";
    public static final String DELETE_FISH = "/rest/fish/delete/{id}";
}
