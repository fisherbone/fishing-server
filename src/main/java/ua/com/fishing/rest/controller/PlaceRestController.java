package ua.com.fishing.rest.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.com.fishing.orm.controller.PlaceRepository;
import ua.com.fishing.orm.entity.Place;

@RestController
@RequestMapping(value = "/rest/place/*", produces = "application/json")
public class PlaceRestController {

    private static final Logger LOG = Logger.getLogger(Place.class.getName());
    @Autowired
    private PlaceRepository placeRep;

    @RequestMapping(value = PlaceRestURIConstants.DUMMY_PLACE, method = RequestMethod.GET)
    public @ResponseBody
    Place getDummyPlace() {
        LOG.log(Level.INFO, "Start getDummyFish");
        Place place = new Place();
        place.setTitle("Dummy");
        return place;
    }

    @RequestMapping(value = PlaceRestURIConstants.GET_PLACE, method = RequestMethod.GET)
    public @ResponseBody
    Place getPlace(@PathVariable("id") int placeId) {
        System.out.println("Start getPlace. ID=" + placeId);
        return placeRep.getById(placeId);
    }

    @RequestMapping(value = PlaceRestURIConstants.GET_COUNT, method = RequestMethod.GET)
    public @ResponseBody
    Integer getCount() {
        System.out.println("Start getCountPlace.");
        return placeRep.getCount();
    }

    @RequestMapping(value = PlaceRestURIConstants.GET_ALL_PLACE, method = RequestMethod.GET)
    public @ResponseBody
    List<Place> getAllPlaces() {
        LOG.log(Level.INFO, "Start getAllPlaces.");
        return placeRep.getAll();
    }

    @RequestMapping(value = PlaceRestURIConstants.GET_PLACE_RANGE, method = RequestMethod.GET)
    public @ResponseBody
    List<Place> getRangePlaces(@PathVariable("first") Integer placeId1, @PathVariable("second") Integer placeId2) {
        LOG.log(Level.INFO, "Start getRangePlaces.");
        return placeRep.getRange(placeId1, placeId2);
    }
    /*
     @RequestMapping(value = PlaceRestURIConstants.CREATE_PLACE, method = RequestMethod.POST)
     public @ResponseBody
     Place createFish(@RequestBody Place place) {
     LOG.log(Level.INFO, "Start createPlace.");
     placeRep.create(place);
     return place;
     }

     @RequestMapping(value = PlaceRestURIConstants.DELETE_PLACE, method = RequestMethod.PUT)
     public @ResponseBody
     Place deleteFish(@PathVariable("id") int placeId) {
     LOG.log(Level.INFO, "Start deletePlace.");
     Place place = placeRep.getById(placeId);
     placeRep.delete(place);
     return place;
     }
     */
}
