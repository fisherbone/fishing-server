/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.fishing.mvc.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import ua.com.fishing.mvc.exception.NotFoundException;

@RequestMapping
@Controller
public class MainController {

    @RequestMapping(value = {"/{path:^index$}"}, method = RequestMethod.GET)
    public ModelAndView getIndex(@PathVariable String path) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("path", path);
        return mav;
    }

    @RequestMapping
    public void get404() throws NotFoundException {
        throw new NotFoundException("Page not found");
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView getNotFound(NotFoundException ex) {
        ModelAndView mav = new ModelAndView("notfound");
        mav.addObject("message", ex.getMessage());
        return mav;
    }
}
