package ua.com.fishing.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import ua.com.fishing.mvc.exception.NotFoundException;
import ua.com.fishing.orm.controller.FishRepository;
import ua.com.fishing.orm.entity.Fish;

@Controller
@RequestMapping({"/fish/*"})
public class FishController {

    @Autowired
    private FishRepository fishRepository;

    @RequestMapping(value = {"/", "/{path:^index$}"}, method = {RequestMethod.GET})
    public ModelAndView getIndex() {
        ModelAndView mav = new ModelAndView("fish/index");
        mav.addObject("fishList", fishRepository.getAll());
        return mav;
    }

    @RequestMapping(value = {"/", "/{item:^\\d+$}"}, method = {RequestMethod.GET})
    public ModelAndView getFish(@PathVariable(value = "item") Integer item_id) throws NotFoundException {
        System.out.println("Into GET METHOD");
        ModelAndView mav = new ModelAndView("fish/info");
        Fish fish = null;
        try {
            fish = fishRepository.getById(item_id);
        } catch (Exception ex) {
            throw new NotFoundException("Page /fish/" + item_id + " not found");
        }
        if (fish != null) {
            System.out.println("Fish place " + fish.getPlaces().size());
            mav.addObject("fish", fish);
        }
        return mav;
    }

    @RequestMapping
    public void get404() throws NotFoundException {
        throw new NotFoundException("Page not found");
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView getNotFound(NotFoundException ex) {
        ModelAndView mav = new ModelAndView("notfound");
        mav.addObject("message", ex.getMessage());
        return mav;
    }
}
