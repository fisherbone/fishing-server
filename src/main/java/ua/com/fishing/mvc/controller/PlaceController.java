package ua.com.fishing.mvc.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import ua.com.fishing.mvc.exception.NotFoundException;
import ua.com.fishing.orm.controller.PlaceRepository;
import ua.com.fishing.orm.entity.Place;

@Controller
@RequestMapping({"/place/*"})
public class PlaceController {
    @Autowired
    private PlaceRepository placeRepository;

    @RequestMapping(value = {"/", "/{path:^index$}"}, method = {RequestMethod.GET})
    public ModelAndView getIndex() {
        ModelAndView mav = new ModelAndView("place/index");
        List<Place> allPlace = placeRepository.getAll();
//        for (Place place : allPlace) {
//            System.out.println("---finding---");
//            System.out.println(place);
//            for (Fish fish : place.getFishs()) {
//            System.out.println("Fishs: \n"+fish);
//            }
//        }
        mav.addObject("placeList", allPlace);
        return mav;
    }

    @RequestMapping(value = {"/", "/{item:^\\d+$}"}, method = {RequestMethod.GET})
    public ModelAndView getPlace(@PathVariable(value = "item") Integer item_id) throws NotFoundException {
        System.out.println("Into GET METHOD");
        ModelAndView mav = new ModelAndView("place/info");
        Place place = null;
        try {
            place = placeRepository.getById(item_id);
        } catch (Exception ex) {
            throw new NotFoundException("Page /place/" + item_id + " not found");
        }
        if (place != null) {
            System.out.println("Place fishs "+place.getFishs().size());
            mav.addObject("place", place);
        }
        return mav;
    }
    
    @RequestMapping
    public void get404() throws NotFoundException {
        throw new NotFoundException("Page not found");
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView getNotFound(NotFoundException ex) {
        ModelAndView mav = new ModelAndView("notfound");
        mav.addObject("message", ex.getMessage());
        return mav;
    }
}
