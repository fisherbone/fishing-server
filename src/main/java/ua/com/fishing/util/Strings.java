package ua.com.fishing.util;

public final class Strings {
    
    /**
     * Return value that indicates null or empty value string. 
     * @param value validation object.
     * @return true if value is null or empty, otherwise false.
     */
    public static boolean isNullOrEmpty(String value) {
        return value == null || "".equals(value);
    }
}
