package ua.com.fishing.orm.controller;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ua.com.fishing.orm.entity.Fish;

@Repository
@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
public class FishRepository {

    @PersistenceContext
    private EntityManager em;

    public EntityManager getManager() {
        return em;
    }

    public List<Fish> getAll() {
        return em.createNamedQuery("Fish.findAll", Fish.class)
                .getResultList();
    }
        public List<Fish> getRange(Integer from, Integer count) {
        return em.createNamedQuery("Fish.findAll", Fish.class)
                .setFirstResult(from)
                .setMaxResults(count)
                .getResultList();
    }
    public Integer getCount() {
        return em.createNamedQuery("Fish.findAll", Fish.class)
                .getResultList().size();
    }
    public Fish getById(Integer id) {
        return em.createNamedQuery("Fish.findById", Fish.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Transactional(readOnly = false)
    public void create(Fish fish) {
        em.persist(fish);
    }

    @Transactional(readOnly = false)
    public Fish update(Fish fish) {
        return em.merge(fish);
    }

    @Transactional(readOnly = false)
    public void delete(Fish fish) {
        em.remove(fish);
    }
}
