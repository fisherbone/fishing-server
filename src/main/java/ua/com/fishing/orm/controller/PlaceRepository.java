package ua.com.fishing.orm.controller;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ua.com.fishing.orm.entity.Place;

@Repository
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class PlaceRepository {

    @PersistenceContext
    private EntityManager em;

    public EntityManager getManager() {
        return em;
    }

    public List<Place> getAll() {
        return em.createNamedQuery("Place.findAll", Place.class)
                .getResultList();
    }
        public List<Place> getRange(Integer id1, Integer id2) {
        return em.createNamedQuery("Place.findRange", Place.class)
                .setParameter("id1", id1)
                .setParameter("id2", id2)
                .getResultList();
    }
    public Integer getCount() {
        return em.createNamedQuery("Place.findAll", Place.class)
                .getResultList().size();
    }
    public Place getById(Integer id) {
        return em.createNamedQuery("Place.findById", Place.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void create(Place place) {
        em.persist(place);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Place update(Place place) {
        return em.merge(place);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void delete(Place place) {
        em.remove(place);
    }
}
