package ua.com.fishing.orm.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "fish_catalog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fish.findAll", query = "SELECT f FROM Fish f"),
    @NamedQuery(name = "Fish.findRange", query = "SELECT f FROM Fish f WHERE f.id >= :id1 and f.id <= :id2"),
    @NamedQuery(name = "Fish.findById", query = "SELECT f FROM Fish f WHERE f.id = :id"),
    @NamedQuery(name = "Fish.findByName", query = "SELECT f FROM Fish f WHERE f.name = :name"),
    @NamedQuery(name = "Fish.findByTranslit", query = "SELECT f FROM Fish f WHERE f.translit = :translit"),
    @NamedQuery(name = "Fish.findByLatName", query = "SELECT f FROM Fish f WHERE f.latName = :latName"),
    @NamedQuery(name = "Fish.findByMidWeight", query = "SELECT f FROM Fish f WHERE f.midWeight = :midWeight"),
    @NamedQuery(name = "Fish.findByMaxWeight", query = "SELECT f FROM Fish f WHERE f.maxWeight = :maxWeight"),
    @NamedQuery(name = "Fish.findByMinAllowedSize", query = "SELECT f FROM Fish f WHERE f.minAllowedSize = :minAllowedSize"),
    @NamedQuery(name = "Fish.findByBestCatchingStart", query = "SELECT f FROM Fish f WHERE f.bestCatchingStart = :bestCatchingStart"),
    @NamedQuery(name = "Fish.findByBestCatchingEnd", query = "SELECT f FROM Fish f WHERE f.bestCatchingEnd = :bestCatchingEnd"),
    @NamedQuery(name = "Fish.findByBestCatchingTime", query = "SELECT f FROM Fish f WHERE f.bestCatchingTime = :bestCatchingTime"),
    @NamedQuery(name = "Fish.findBySpecConditions", query = "SELECT f FROM Fish f WHERE f.specConditions = :specConditions"),
    @NamedQuery(name = "Fish.findByBestWatTemperature", query = "SELECT f FROM Fish f WHERE f.bestWatTemperature = :bestWatTemperature"),
    @NamedQuery(name = "Fish.findBySpawnTemperature", query = "SELECT f FROM Fish f WHERE f.spawnTemperature = :spawnTemperature"),
    @NamedQuery(name = "Fish.findBySpawnStart", query = "SELECT f FROM Fish f WHERE f.spawnStart = :spawnStart"),
    @NamedQuery(name = "Fish.findBySpawnEnd", query = "SELECT f FROM Fish f WHERE f.spawnEnd = :spawnEnd")})
public class Fish implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "translit")
    private String translit;
    @Column(name = "lat_name")
    private String latName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "mid_weight")
    private BigDecimal midWeight;
    @Column(name = "max_weight")
    private BigDecimal maxWeight;
    @Column(name = "min_allowed_size")
    private Integer minAllowedSize;
    @Column(name = "best_catching_start")
    private String bestCatchingStart;
    @Column(name = "best_catching_end")
    private String bestCatchingEnd;
    @Column(name = "best_catching_time")
    private String bestCatchingTime;
    @Column(name = "spec_conditions")
    private String specConditions;
    @Column(name = "best_wat_temperature")
    private Integer bestWatTemperature;
    @Column(name = "spawn_temperature")
    private Integer spawnTemperature;
    @Column(name = "spawn_start")
    private String spawnStart;
    @Column(name = "spawn_end")
    private String spawnEnd;
    @JsonIgnore
    @XmlTransient
    @Basic(optional = false)
    @Lob
    @Column(name = "image")
    private byte[] image;
    @ManyToMany(mappedBy = "fishs")
    private List<Place> places;

    public Fish() {
    }

    public Fish(Integer id) {
        this.id = id;
    }

    public Fish(Integer id, String name, String translit, byte[] image) {
        this.id = id;
        this.name = name;
        this.translit = translit;
        this.image = image;
    }

    @JsonBackReference
    public List<Place> getPlaces() {
        return this.places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTranslit() {
        return translit;
    }

    public void setTranslit(String translit) {
        this.translit = translit;
    }

    public String getLatName() {
        return latName;
    }

    public void setLatName(String latName) {
        this.latName = latName;
    }

    public BigDecimal getMidWeight() {
        return midWeight;
    }

    public void setMidWeight(BigDecimal midWeight) {
        this.midWeight = midWeight;
    }

    public BigDecimal getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(BigDecimal maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Integer getMinAllowedSize() {
        return minAllowedSize;
    }

    public void setMinAllowedSize(Integer minAllowedSize) {
        this.minAllowedSize = minAllowedSize;
    }

    public String getBestCatchingStart() {
        return bestCatchingStart;
    }

    public void setBestCatchingStart(String bestCatchingStart) {
        this.bestCatchingStart = bestCatchingStart;
    }

    public String getBestCatchingEnd() {
        return bestCatchingEnd;
    }

    public void setBestCatchingEnd(String bestCatchingEnd) {
        this.bestCatchingEnd = bestCatchingEnd;
    }

    public String getBestCatchingTime() {
        return bestCatchingTime;
    }

    public void setBestCatchingTime(String bestCatchingTime) {
        this.bestCatchingTime = bestCatchingTime;
    }

    public String getSpecConditions() {
        return specConditions;
    }

    public void setSpecConditions(String specConditions) {
        this.specConditions = specConditions;
    }

    public Integer getBestWatTemperature() {
        return bestWatTemperature;
    }

    public void setBestWatTemperature(Integer bestWatTemperature) {
        this.bestWatTemperature = bestWatTemperature;
    }

    public Integer getSpawnTemperature() {
        return spawnTemperature;
    }

    public void setSpawnTemperature(Integer spawnTemperature) {
        this.spawnTemperature = spawnTemperature;
    }

    public String getSpawnStart() {
        return spawnStart;
    }

    public void setSpawnStart(String spawnStart) {
        this.spawnStart = spawnStart;
    }

    public String getSpawnEnd() {
        return spawnEnd;
    }

    public void setSpawnEnd(String spawnEnd) {
        this.spawnEnd = spawnEnd;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    /*
     public String getImageBase64() {
     String encodeBase64String = Base64.encodeBase64String(image);
     return encodeBase64String;
     }
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fish other = (Fish) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Fish{" + "id=" + id + ", name=" + name + ", translit=" + translit + ", latName=" + latName + ", midWeight=" + midWeight + ", maxWeight=" + maxWeight + ", minAllowedSize=" + minAllowedSize + ", bestCatchingStart=" + bestCatchingStart + ", bestCatchingEnd=" + bestCatchingEnd + ", bestCatchingTime=" + bestCatchingTime + ", specConditions=" + specConditions + ", bestWatTemperature=" + bestWatTemperature + ", spawnTemperature=" + spawnTemperature + ", spawnStart=" + spawnStart + ", spawnEnd=" + spawnEnd + ", places=" + places + '}';
    }

}
